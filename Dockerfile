FROM ghcr.io/ansible/awx_devel:HEAD

RUN mkdir -p /etc/tower/conf.d /etc/nginx /etc/receptor

COPY awx /awx_devel
COPY awx/tools/docker-compose/supervisor.conf /etc/supervisord.conf
COPY awx/tools/docker-compose/_sources/database.py /etc/tower/conf.d/database.py
COPY awx/tools/docker-compose/_sources/websocket_secret.py /etc/tower/conf.d/websocket_secret.py
COPY awx/tools/docker-compose/_sources/local_settings.py /etc/tower/conf.d/local_settings.py
COPY awx/tools/docker-compose/_sources/nginx.conf /etc/nginx/nginx.conf
COPY awx/tools/docker-compose/_sources/nginx.locations.conf /etc/nginx/conf.d/nginx.locations.conf
COPY awx/tools/docker-compose/_sources/SECRET_KEY /etc/tower/SECRET_KEY
COPY awx/tools/docker-compose/_sources/receptor/receptor-awx-1.conf /etc/receptor/receptor.conf
COPY awx/tools/docker-compose/_sources/receptor/receptor-awx-1.conf.lock /etc/receptor/receptor.conf.lock

COPY configuration/local_settings.py /etc/tower/conf.d/local_settings.py
COPY configuration/uwsgi.ini /etc/tower/uwsgi.ini
COPY configuration/supervisord.conf /etc/supervisord.conf

RUN chown -R 1000 /awx_devel
RUN chown -R 1000 /etc/tower
RUN chown -R 1000 /var/lib/awx
RUN chown 1000 /etc/nginx/nginx.conf /etc/nginx/conf.d/nginx.locations.conf /etc/receptor/receptor.conf /etc/supervisord.conf
