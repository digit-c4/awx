# awx

This repository builds an container image for awx with all the needed compenents inside the container.

To use this docker-compose, you need to set the 
- NO_PROXY 
- HTTPS_PROXY 
- HTTP_PROXY 
- DJANGO_SUPERUSER_PASSWORD 
- POSTGRES_DEFAULT_PASSWORD 
- AWX_SECRET_KEY

environment variable.

To upgrade the container, you will need to keep the secret file, mount point /etc/tower/SECRET_KEY
